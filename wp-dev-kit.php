<?php
/**
 * Plugin Name: WordPress Development Kit Plugin
 * Plugin URI: https://wordpress.storelocatorplus.com/product/wordpress-development-kit-plugin/
 * Description: Show updates for SLP add ons and validate premier license.
 * Author: Store Locator Plus®
 * Author URI: https://wordpress.storelocatorplus.com/
 * Requires at least: 6.4.1
 * Tested up to : 6.4.1
 * Version: 2311.17.02
 *
 * Text Domain: wp-dev-kit
 * Domain Path: /languages/
 *
 */

define( 'WPDK__VERSION', '2311.17.02' );
define( 'WPDK__PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
define( 'WPDK__PLUGIN_FILE', __FILE__ );

require_once( WPDK__PLUGIN_DIR . 'include/wpdkPlugin.php' );

register_activation_hook( WPDK__PLUGIN_FILE, array( 'wpdkPlugin', 'plugin_activation' ) );

add_action( 'init', array( 'wpdkPlugin', 'init' ) );
add_action( 'wp_ajax_wpdk_download_file', array( 'wpdkPlugin', 'download_file' ) );
add_action( 'wp_ajax_nopriv_wpdk_download_file', array( 'wpdkPlugin', 'download_file' ) );
add_action( 'wp_ajax_wpdk_updater', array( 'wpdkPlugin', 'updater' ) );
add_action( 'wp_ajax_nopriv_wpdk_updater', array( 'wpdkPlugin', 'updater' ) );

