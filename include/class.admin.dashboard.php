<?php
if (! class_exists('wpdkPlugin_Dashboard')) {

    /**
     * Holds the admin-only Dashboard code.
     *
     * @package wpdkPlugin\Admin\Dashboard
     * @author Lance Cleveland <lance@charlestonsw.com>
     * @copyright 2016 Charleston Software Associates, LLC
     */
    class wpdkPlugin_Dashboard extends WPDK_BaseClass_Object {

        /**
         * Invoke the Dashboard object.
         */
        function initialize() {
            add_action( 'wp_dashboard_setup' , array( $this , 'add_all_widgets' ) );
        }

        /**
         * Add all WPDK widgets.
         */
        function add_all_widgets() {
            wp_add_dashboard_widget( 'wpdk_recent_updates' , __( 'WPDK Most Recent Updates' , 'wp-dev-kit' ) , array( $this, 'recent_updates' ) );
        }

        /**
         * Render the recent updates widget.
         */
        function recent_updates() {
            $limit = $this->addon->options['update_history_limit'];
            $this->addon->create_object_Database();

            $update_history = (array) $this->addon->Database->fetch_recent_history( $limit );

            // No history
            //
            if ( is_null( $update_history ) ) {
                echo __('No update history.', 'wp-dev-kit');
                return;
            }

            echo '<div class="rsswidget wpdk update_history"><ul>';
            $last_url = '';
            foreach ( $update_history as $update_entry ) {
                echo '<li>';
                if ( $last_url !== $update_entry['site_url'] ) {
                    echo "<a class='rsswidget' href='{$update_entry['site_url']}' target='csa'>{$update_entry['site_url']}</a>";
                    echo " <span class='rss-date'>{$update_entry['lastupdated']}</span>";
                    $last_url = $update_entry['site_url'];
                }
                ?>
                    <div class="rssSummary">
                        <?= $update_entry['slug']; ?>
                        <?php _e( ' is on ' , 'wp-dev-kit' ); ?><?= $update_entry['current_version']; ?>
                        <?php
                            if ( ! version_compare( $update_entry['current_version'] , $update_entry['new_version'] , '==' ) ) {
                                echo
                                    ' ' .
                                    $update_entry['target'] .
                                    __( ' now serving ' , 'wp-dev-kit' ) .
                                    $update_entry['new_version']
                                    ;
                            }
                            if ( $update_entry['action'] === 'send_file' ) {
                                printf( __( ' validation failed for uid: %s sid: %s ' , 'wp-dev-kit' ) , $update_entry['uid'] , $update_entry['sid'] );
                            }
                        ?>
                    </div>
                <?php
                echo '</li>';
            }
            echo '</ul></div>';
        }
    }
}
