<?php

/**
 * Class WPDK_BaseClass_Object
 *
 * @property-read    wpdkPlugin $addon;
 */
class WPDK_BaseClass_Object {
	protected $addon;

	/**
	 * @param array $options
	 */
	function __construct( $options = array() ) {
		$this->set_properties( $options );
		
		$this->addon = wpdkPlugin::init();

		$this->initialize();
	}

	/**
	 * @param string $property
	 *
	 * @return bool
	 */
	function __isset( $property ) {
		return isset( $this->$property );
	}


	/**
	 * Do these things when this object is invoked.
	 */
	protected function initialize() {
		// Override with anything you want to run when your extension is invoked.
	}

	/**
	 * Set our properties.
	 *
	 * @param array $options
	 */
	public function set_properties( $options = array() ) {
		if ( ! empty( $options ) && is_array( $options ) ) {
			foreach ( $options as $property => $value ) {
				if ( property_exists( $this, $property ) ) {
					$this->$property = $value;
				}
			}
		}
	}
}